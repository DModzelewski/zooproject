package pl.codementors.zoo.Animals.Species;

import pl.codementors.zoo.Animals.Interfaces.Carnivourous;
import pl.codementors.zoo.Animals.Mammal;

public class Wolf extends Mammal implements Carnivourous{
    private String name;

    public void howl(){
        System.out.println(getName()+" howls");
    }
    @Override
    public void eat(){
        System.out.println(getName()+" devours");
    }

    @Override
    public void eatMeat() {
        System.out.println("A piece of meat is given to "+getName());
        eat();
    }
}
