package pl.codementors.zoo.Animals.Species;

import pl.codementors.zoo.Animals.Bird;
import pl.codementors.zoo.Animals.Interfaces.Herbivorous;

public class Parrot extends Bird implements Herbivorous{
    private String name;

    public void screetch(){
        System.out.println(getName()+ " screetches");
    }
    @Override
    public void eat(){
        System.out.println(getName()+" pecks");
    }

    @Override
    public void eatPlant() {
        System.out.println("A plant is given to "+getName());
        eat();
    }
}
