package pl.codementors.zoo.Animals.Species;

import pl.codementors.zoo.Animals.Interfaces.Herbivorous;
import pl.codementors.zoo.Animals.Lizard;

public class Iguana extends Lizard implements Herbivorous{
    private String name;

    public void hiss(){
        System.out.println(getName()+" hisses");
    }
    @Override
    public void eat(){
        System.out.println(getName()+" swallows");
    }

    @Override
    public void eatPlant() {
        System.out.println("A plant is given to "+getName());
        eat();
    }
}
