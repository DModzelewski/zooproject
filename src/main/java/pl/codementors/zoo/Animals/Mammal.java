package pl.codementors.zoo.Animals;

import pl.codementors.zoo.Animal;

public abstract class Mammal extends Animal{
    private String furColor;

    public String getFurColor() {return furColor;}
    public void setFurColor(String furColor) {this.furColor = furColor;}
}
