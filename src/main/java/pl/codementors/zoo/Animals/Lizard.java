package pl.codementors.zoo.Animals;

import pl.codementors.zoo.Animal;

public abstract class Lizard extends Animal{
    private String scaleColor;

    public String getScaleColor() {return scaleColor;}
    public void setScaleColor(String scaleColor) {this.scaleColor = scaleColor;}
}
