package pl.codementors.zoo.Animals.Interfaces;

public interface Carnivourous {
    void eatMeat();
}
