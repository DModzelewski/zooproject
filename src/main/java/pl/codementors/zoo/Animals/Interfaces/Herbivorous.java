package pl.codementors.zoo.Animals.Interfaces;

public interface Herbivorous {
    void eatPlant();
}
