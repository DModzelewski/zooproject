package pl.codementors.zoo.Animals;

import pl.codementors.zoo.Animal;

public abstract class Bird extends Animal {
    private String featherColor;

    public String getFeatherColor() {return featherColor;}
    public void setFeatherColor(String featherColor) {this.featherColor = featherColor;}
}
