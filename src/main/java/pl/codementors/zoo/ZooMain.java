package pl.codementors.zoo;

import pl.codementors.zoo.Animals.Bird;
import pl.codementors.zoo.Animals.Interfaces.Carnivourous;
import pl.codementors.zoo.Animals.Interfaces.Herbivorous;
import pl.codementors.zoo.Animals.Lizard;
import pl.codementors.zoo.Animals.Mammal;
import pl.codementors.zoo.Animals.Species.Iguana;
import pl.codementors.zoo.Animals.Species.Parrot;
import pl.codementors.zoo.Animals.Species.Wolf;

import java.io.*;
import java.util.Scanner;

public class ZooMain implements Serializable{
    public static void main(String[] args) {
        String file = "AnimalList"; // nazwa pliku w którym zapisana będzie lista zwierząt
        String binaryFile = "binaryAnimalList"; //nazwa pliku binarnego
        System.out.println("Hello to the ZOO");
        System.out.println("How many animals will you keep?");
        int capacity;
        Scanner input = new Scanner(System.in);

        capacity = input.nextInt();
        Animal [] animals = new Animal[capacity];

        int i; //iterator
        for (i = 0; i < capacity; i++) {
            System.out.println("Animal type: ");
            System.out.println("1 - Wolf");
            System.out.println("2 - Iguana");
            System.out.println("3 - Parrot");
            int type = input.nextInt();
            animals[i] = null;
            switch (type) {
                case 1: {
                    animals[i] = new Wolf();
                    break;
                }
                case 2: {
                    animals[i] = new Iguana();
                    break;
                }
                case 3: {
                    animals[i] = new Parrot();
                    break;
                }
            }
            if (animals[i] != null) {
                System.out.println("What is its name?");
                String name = input.next();
                animals[i].setName(name);

                System.out.println("How old is it?");
                int age = input.nextInt();
                animals[i].setAge(age);

                System.out.println("What color is it?");
                String color = input.next();
                if (animals[i] instanceof Mammal) {
                    ((Mammal) animals[i]).setFurColor(color);
                } else if (animals[i] instanceof Bird) {
                    ((Bird) animals[i]).setFeatherColor(color);
                } else if (animals[i] instanceof Lizard) {
                    ((Lizard) animals[i]).setScaleColor(color);
                } else {
                    System.out.println("Something went terribly wrong");
                }

            }
        }

        print(animals);
        System.out.println("***");
        feed(animals);
        System.out.println("***");
        howl(animals);
        hiss(animals);
        screech(animals);
        System.out.println("***");
        feedWithMeat(animals);
        System.out.println("***");
        feedWithPlants(animals);
        System.out.println("***");
        saveToFile(animals, file);
        System.out.println("***");
        readFromFile(file);
        System.out.println("***");
        printColors(animals);
        System.out.println("***");
        saveToBinaryFile(animals, binaryFile);
        System.out.println("***");
        readFromBinaryFile(binaryFile);


    }


    private static void print(Animal[] animals) {
        System.out.println("Your animals:");
        for (Animal i : animals) {
            System.out.print(i.getClass().getSimpleName() + ": " + i.getName() + "\n");
        }
    }

    private static void feed(Animal[] animals) {
        System.out.println("You're feeding animals:");
        for (Animal i : animals) {
            i.eat();
        }
    }

    private static void saveToFile(Animal[] animals, String file) {
        System.out.println("Saving animals to file");
        File example = new File("/tmp/" + file + ".txt");
        FileWriter fw = null;
        try {
            fw = new FileWriter(example);
            for (Animal i : animals) {
                fw.write(i.getClass().getSimpleName() + ": " + i.getName() + "\n");
            }
        } catch (Exception ex) {
            System.out.println(ex);
        } finally {
            if (fw != null) {
                try {
                    fw.close();
                    System.out.println("Saved");
                } catch (IOException ex2) {
                    System.out.println(ex2);
                }
            }
        }
    }

    private static void howl(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Wolf) {
                System.out.println(i.getName() + " howls!");
            }
        }
    }

    private static void hiss(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Iguana) {
                System.out.println(i.getName() + " hisses!");
            }
        }
    }

    private static void screech(Animal[] animals) {
        for (Animal i : animals) {
            if (i instanceof Parrot) {
                System.out.println(i.getName() + " screeches!");
            }
        }
    }

    private static void feedWithMeat(Animal[] animals) {
        for (Animal i : animals){
            if (i instanceof Carnivourous){
                ((Carnivourous) i).eatMeat();
            }
        }
    }

    private static void feedWithPlants(Animal[] animals) {
        for (Animal i : animals){
            if (i instanceof Herbivorous){
                ((Herbivorous) i).eatPlant();
            }
        }
    }

    private static void readFromFile(String file){
        BufferedReader br = null;
        FileReader fr = null;
        System.out.println("Loading from "+file+" to tempArray");
        int i=0;
        try{
            fr = new FileReader("/tmp/" +file+ ".txt");
            br = new BufferedReader(fr);
            while(br.ready()){
                br.readLine();
                i++;
            }
        } catch (Exception ex){
            System.out.println(ex);
        } finally {
            try {
                br.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        String[] tempArray = new String[i];
        try{
            fr = new FileReader("/tmp/"+file+ ".txt");
            br = new BufferedReader(fr);
            for(int i2=0; i2<i; i2++){
                String line = br.readLine();
                tempArray[i2] = line;
            }
        }catch (Exception ex){
            System.out.println(ex);
        }finally {
            try{
                br.close();
            } catch (Exception e){
                System.out.println(e);
            }
        }
        Scanner input = new Scanner(System.in);
        System.out.println("Which array element do you want to load? 0 to "+(i-1)+" only!");
        System.out.println("(-1 to load all elements)");
        int command = input.nextInt();
        if (command < -1){
            System.out.println("Wrong input.");
        } else if (command == -1){
            for(String element : tempArray){
                System.out.println(element);
            }
        } else {
            System.out.println(tempArray[command]);
        }

    }

    private static void printColors(Animal[] animals){
        System.out.println("Animals' colors:");
        for (Animal i : animals){
            String color = "none";
            if (i instanceof Mammal){
                color = ((Mammal) i).getFurColor();
            } else if (i instanceof Lizard){
                color = ((Lizard) i).getScaleColor();
            } else if (i instanceof Bird){
                color = ((Bird) i).getFeatherColor();
            } else {
                System.out.println("That's impossible");
            }
            System.out.println(i.getName()+": "+color);
        }
    }

    private static void saveToBinaryFile(Animal[] animals, String binaryFile){
        File bin = new File("/tmp/"+binaryFile);
        System.out.println("Saving to binary");
        try {
            FileOutputStream fos = new FileOutputStream(bin);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            for (Animal i : animals) {
                oos.writeObject(i.getClass().getSimpleName() + ": " + i.getName());
            }
        } catch (IOException ex){
            System.out.println(ex);
        }

    }

    private static void readFromBinaryFile(String binaryFile){
        System.out.println("Reading from binary");
        int i = 0; //iterator
        try {
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("/tmp/"+binaryFile)));
            while(ois!=null) {
                String input = (String) ois.readObject();
                i++;
            }
        } catch (Exception ex){
            System.out.println("Creating array...");
        }

        String[] tempArray = new String[i];

        try{
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("/tmp/"+binaryFile)));
            for (int i2=0; i2<i; i2++){
                String input = (String) ois.readObject();
                tempArray[i2] = input;
            }
        }catch (Exception ex){
            System.out.println("End of the file");
        }finally {
            System.out.println("Saved array:");
            for (String s : tempArray){
                System.out.println(s);
            }
        }
    }
}
